`nvn.lua` is a DCS game server script to reject/limit slot selection based on N v N restriction set by a trigger zone.

## Install the script to your server
	- copy the script `nvn.lua` to your DCS Game Configuration's `\Scripts\Hooks\` folder. This is usually `%userprofile%\Saved Games\DCS\Scripts\Hooks` or `C:\Users\<your_user_name>\Saved Games\DCS\Scripts\Hooks`.

## Modify your multiplayer missions to restrict a zone to N vs N
	- Add planes as usual
	- Create trigger zone to cover an area of planes. Only their initial positions are used to determine whether a plane belongs to a zone.
		- You can use existing zone if they happen to cover appropriate area.
		- Only circular zone is supported. Rectangular zone is not yet supported now.
	- In the zone editing panel
		- Add a property named "Limit_NvN".
		- Set the value to your N vs N limit, such as 1 for 1v1.
	- Save and close mission editor.

## To estrict a zone to only certain users with known UCID
	- In mission editor
		- Create a circular zone, cover initial positions of planes you want to restrict.
		- Create a zone property called Require_UCID_Group, and value as a ucid list name of your choices. "dojo", for example.
	- In this `nvn.lua` script under `\Scripts\Hooks\` folder,
		- Edit the values within the UCID_GROUPS variable. Example:

```
local UCID_GROUPS = {
    ["dojo"] = {
        "022ad9bd90ddee2a54be6ff255677890", -- add comment like this so you remember whose is this
        "a54be6ff207eae4d463fd2b5ea012345", -- one id per line, remember the quotes and comma.
    },
    ["another group like this"] = {  -- you can add another group like this, so you can label some zones to be only usable by another list of users
        "76d2bfe7346693b022ad9bd90dde1234",
        "be6ff207eae4d463fd2b5ea098765432",
    },
}
```

Alternatively, if you need to frequently change UCID_GROUPS without wanting to restart DCS, then, you can use a separate file for allowed UCIP groups.

Set ```USE_EXTERNAL_UCID_GROUPS_TXT_FILE_ENABLED``` to true, and set ```USE_EXTERNAL_UCID_GROUPS_TXT_FILE_PATH``` to the file's location (remember to use double backslash).

Example settings in nvn.lua:


```
local USE_EXTERNAL_UCID_GROUPS_TXT_FILE_ENABLED = true
local USE_EXTERNAL_UCID_GROUPS_TXT_FILE_PATH = "c:\\tmp\\allowed_dcs_ucid_groups.txt"
local USE_EXTERNAL_UCID_GROUPS_TXT_FILE_RELOAD_INTERVAL = 10
```

Example allowed_dcs_ucid_groups.txt content:

```
dojo
022ad9bd90ddee2a54be6ff255677890, someone
a54be6ff207eae4d463fd2b5ea012345, xxx
another group like this
76d2bfe7346693b022ad9bd90dde1234, must have something here
be6ff207eae4d463fd2b5ea098765432, anything as your note

```
