-- This is a DCS game server script to reject/limit slot selection based on N v N restriction set by a trigger zone.
-- Put this script in your ```Saved Games\DCS_or_whatever_dcs_config_folder\Scripts\Hooks\``` folder.
-- In your multiplayer mission, create a Trigger Zone to cover all planes you want to limit slot selection to N vs N.
-- In the zone's properties, add one named Limit_NvN, this is Case Sensitive. And set the value to 1 for 1v1, 2 for 2v2, etc...
-- If you encounter issues, set the DEBUG_LOG_ENABLED to true so more debug output will go to Saved Games\DCS\Logs\dcs.log
-- If DEBUG_OTHER_FILE_OUTPUT_ENABLED is set true, the script will also try to write some debug files to C:\tmp\ folder. If this folder does not exist, then the script may fail.
-- ### END OF DOCUMENTATION ### --
--
-- #######################################################################################################################################################
--
-- ### BEGIN OF CONFIGURATION ### --
local ZONE_NVN_PROPERTY_NAME = "Limit_NvN" -- You create a trigger zone and give it a property with this name, set its value as number of players allowed on one side.
local ZONE_REQUIRE_UCID_PROPERTY_NAME = "Require_UCID_Group" -- if a trigger zone has this property, its value will be used to retrieve a list of allowed UCIDs from the UCID_GROUPS table below.
local ZONE_UCID_REJECT_TEXT = "Zone_UCID_Reject_Text" -- if used, when rejecting a user with unknown UCID, they get this property's value as the message instead of standard "slot disabled" message.
local ZONE_NVN_REJECT_TEXT = "Zone_NvN_Reject_Text" -- if used, when rejecting a user because of N vs N limit, they get this property's value as the message instead of standard "slot disabled" message.

local USE_EXTERNAL_UCID_GROUPS_TXT_FILE_ENABLED = true  -- if true then we will use new content to fully replace UCID_GROUPS variable.
local USE_EXTERNAL_UCID_GROUPS_TXT_FILE_PATH = "c:\\tmp\\allowed_dcs_ucid_groups.txt"  -- file cotent: Each line must contain UCID, a comma, and a player name. If comma not found then it is considered a name of a new group and start position of that group.
local USE_EXTERNAL_UCID_GROUPS_TXT_FILE_RELOAD_INTERVAL = 10  -- integer, seconds, minimum time between a file reload.

local UCID_GROUPS = { -- Groups of lists of allowed UCIDs. May be replaced with instructions in external txt file.
    ["dojo"] = {
        "d90ddee2a54be6ff207eae4d463fd2b5", -- UCID of allowed user, put next one in quotes in new line, end with a comma.
    },
    ["69th squadron"] = {  -- add new group like this.
        "somebodyucid",
        "ucidsomeoneElse",
    }
}

local DEBUG_LOG_ENABLED = false -- true to enable more messy debug output, if you encounter some problems and need to collect more info.
local DEBUG_OTHER_FILE_OUTPUT_ENABLED = false -- true to write more debug objects to some files in c:\tmp\ folder. You have to create this folder first to avoid script crash.

local USER_UCID_LOGGING_TO_MAIN_DCS_LOG = true -- whether we log user/ucid relationship within main dcs.log. This is totally separate and not affected by DEBUG_LOG_ENABLED setting.
local SEPARATE_USER_UCID_LOGGING_ENABLED = true -- if true, then user's slot selection attempt will write a new line of playerName / UCID relationship into the specified location.
local SEPARATE_USER_UCID_LOGGING_FILE_PATH = "c:\\tmp\\dcs_user_ucid_log.txt" -- where to append the user/ucid info, if enabled.

-- ### END OF CONFIGURATION ### --
--
-- #######################################################################################################################################################
--

local NvN = { -- DONT REMOVE!!!
    ["knownZones"] = {},
    ["lastUcidGroupReloadTime"] = nil,
} -- DONT REMOVE!!!

local function trim(s)
    return string.gsub(s, "^%s*(.-)%s*$", "%1")
end

local function loadUcidFile(file) -- will close
    log.write('NvN.LUA', log.INFO, 'loadUcidFile')
    if not file then
        return {}
    end
    local count = 0
    local ret = {}
    local groupName = nil
    for line in file:lines() do
        line = trim(line)
        if line then
            local commaPosition = string.find(line, ',')
            if commaPosition then
                local ucid = trim(string.sub(line, 1, commaPosition - 1))
                if groupName and ucid then
                    local group = ret[groupName]
                    if group == nil then
                        ret[groupName] = {}
                        group = ret[groupName]
                    end
                    table.insert(group, ucid)
                    count = count + 1
                end
            else
                groupName = line
            end
        end
    end
    log.write('NvN.LUA', log.INFO, 'Reloaded UCID file and found ' .. count)
    file:close()
    return ret
end


local function dump(o) -- for debug output
    if type(o) == 'table' then
        local s = '{ '
        for k, v in pairs(o) do
            if type(k) ~= 'number' then
                k = '"' .. k .. '"'
            end
            s = s .. '[' .. k .. '] = ' .. dump(v) .. ','
        end
        return s .. '} '
    else
        if type(o) == 'number' then
            return tostring(o)
        end
        return '"' .. tostring(o) .. '"'
    end
end

local function fileOut(filePath, data)
    local out_file = io.open(filePath, "w+")
    out_file:write(dump(data))
    out_file:close()
end

local function fileOutMessyDebug(filePath, data)
    if DEBUG_OTHER_FILE_OUTPUT_ENABLED then
        fileOut(filePath, data)
    end
end

local function fileOutAppendLine(filePath, line)
    local out_file = io.open(filePath, "a")
    out_file:write(line)
    out_file:write("\r\n")
    out_file:close()
end

local function logUserUcid(user, ucid)
    if USER_UCID_LOGGING_TO_MAIN_DCS_LOG then
        log.write('NvN.LUA', log.INFO, 'UCID: [' .. ucid .. '] ' .. user)
    end
    if SEPARATE_USER_UCID_LOGGING_ENABLED then
        fileOutAppendLine(SEPARATE_USER_UCID_LOGGING_FILE_PATH, os.date('%Y-%m-%d %H:%M:%S ') .. ucid .. ' ' .. user)
    end
end

local function logWriteMessyDebug(txt)
    if DEBUG_LOG_ENABLED then
        log.write('NvN.LUA', log.INFO, txt)
    end
end

local SIDE_TO_COALITION_NAME = {
    [1] = "red",
    [2] = "blue",
    ["1"] = "red",
    ["2"] = "blue",
    ["red"] = "red",
    ["blue"] = "blue"
}

local function _getZoneProp(zone, name)
    logWriteMessyDebug("getting name=" .. name)
    -- logWriteMessyDebug("zone.properties=" .. dump(zone.properties))
    for _, obj in pairs((zone and zone.properties) or {}) do
        -- logWriteMessyDebug("obj=" .. dump(obj))
        if obj.key == name then
            return obj.value
        end
    end
    logWriteMessyDebug("end for")
    return nil
end

local function distSquared(a, b)
    local x_dist = a.x - b.x
    local y_dist = a.y - b.y
    return x_dist * x_dist + y_dist * y_dist
end

local function isUnitInZone(unit, zone)
    -- logWriteMessyDebug("isUnitInZone: zone._radiusSquared" .. dump(zone._radiusSquared))
    return distSquared(unit, zone) <= tonumber(zone._radiusSquared)
end

local function isItemInList(item, list)
    for _, listItem in pairs(list or {}) do
        if listItem == item then
            return true
        end
    end
    return false
end

NvN.reloadExternalUcidGroups = function()
    if not USE_EXTERNAL_UCID_GROUPS_TXT_FILE_ENABLED then
        return
    end
    if NvN.lastUcidGroupReloadTime ~= nil and NvN.lastUcidGroupReloadTime + USE_EXTERNAL_UCID_GROUPS_TXT_FILE_RELOAD_INTERVAL > os.time() then
        return --  not time to check yet.
    end
    local file = io.open(USE_EXTERNAL_UCID_GROUPS_TXT_FILE_PATH, 'r')
    if file == nil then
        log.write('NvN.LUA', log.INFO, 'FAILED trying to open SEPARATE_USER_UCID_LOGGING_FILE_PATH' .. USE_EXTERNAL_UCID_GROUPS_TXT_FILE_PATH)
        return
    end
    UCID_GROUPS = loadUcidFile(file)
    NvN.lastUcidGroupReloadTime = os.time()
end

NvN.onMissionLoadEnd = function()
    logWriteMessyDebug("onMissionLoadEnd call 0")
    if not DCS.isServer() or not DCS.isMultiplayer() then
        return
    end
    local mission = DCS.getCurrentMission().mission
    -- logWriteMessyDebug((dump(mission)))
    logWriteMessyDebug("onMissionLoadEnd call")
    NvN.mission = mission
    fileOutMessyDebug("c:\\tmp\\dcs_mission_out.lua", NvN.mission)
    NvN.knownZones = {}
    logWriteMessyDebug("onMissionLoadEnd call 0001")

    for _, zone in pairs(mission.triggers.zones or {}) do
        logWriteMessyDebug("onMissionLoadEnd call 0020")
        local limitNvN = tonumber(_getZoneProp(zone, ZONE_NVN_PROPERTY_NAME))
        local ucidGroupName = _getZoneProp(zone, ZONE_REQUIRE_UCID_PROPERTY_NAME)
        logWriteMessyDebug("onMissionLoadEnd call 0030")
        if limitNvN ~= nil or ucidGroupName then
            logWriteMessyDebug("onMissionLoadEnd call 0040")
            NvN.knownZones[zone.zoneId] = zone
            zone._radiusSquared = zone.radius * zone.radius
            zone.limitNvN = limitNvN
            zone.ucidGroupName = ucidGroupName
            zone.ucidRejectText = _getZoneProp(zone, ZONE_UCID_REJECT_TEXT)
            zone.nvnRejectText = _getZoneProp(zone, ZONE_NVN_REJECT_TEXT)
            logWriteMessyDebug("onMissionLoadEnd call 0050")
        end
        logWriteMessyDebug("onMissionLoadEnd call 0100")
    end
    fileOutMessyDebug("c:\\tmp\\dcs_zones_out.lua", NvN.knownZones)

    NvN.missionUnits = {}

    for coalitionName, coalition in pairs(mission.coalition) do
        logWriteMessyDebug("b")
        for _, country in pairs(coalition.country) do
            logWriteMessyDebug("c")
            for _, group in pairs(country.plane.group) do
                logWriteMessyDebug("d")
                for _, unit in pairs(group.units) do
                    logWriteMessyDebug("E")
                    NvN.missionUnits[unit.unitId] = unit
                    unit.inZones = {}
                    logWriteMessyDebug("f")
                    for _, zone in pairs(NvN.knownZones) do
                        logWriteMessyDebug("g")
                        if isUnitInZone(unit, zone) then
                            logWriteMessyDebug("g1")
                            if zone.coveredUnits == nil then
                                logWriteMessyDebug("g2")
                                zone.coveredUnits = {
                                    ["blue"] = {},
                                    ["red"] = {}
                                }
                            end
                            local coveredUnits = zone.coveredUnits
                            coveredUnits[coalitionName][unit.unitId] = unit
                            unit.inZones[zone.zoneId] = true
                        else
                            logWriteMessyDebug("g2 (else)")
                        end
                    end
                end
            end
        end
    end
    fileOutMessyDebug("c:\\tmp\\dcs_NvNmissionUnits.lua", NvN.missionUnits)
    logWriteMessyDebug("done reading mission2")
end

NvN.getMissionUnitBySlotId = function(slotId)
    return NvN.missionUnits[tostring(slotId)] or NvN.missionUnits[tonumber(slotId)] -- too lazy to bother make them consistent.
end

NvN.getSameSidePlayersInZone = function(side, zoneId, minusPlayerId)
    logWriteMessyDebug("getSameSidePlayersInZone 1")
    local result = {}
    local coalitionName = SIDE_TO_COALITION_NAME[side]
    local players = net.get_player_list()
    for _, playerId in pairs(players) do
        local theirSide, theirSlotId = net.get_slot(playerId)
        local otherPlayer = net.get_player_info(playerId)
        if SIDE_TO_COALITION_NAME[otherPlayer.side] == coalitionName and otherPlayer.slot and tonumber(playerId) ~=
            tonumber(minusPlayerId) and not string.find(otherPlayer.slot, "_") then -- note that we ignore any slot that contains underscore, which may indicate a second seat on one plane.
            local unit = NvN.getMissionUnitBySlotId(otherPlayer.slot)
            if unit and unit.inZones and unit.inZones[zoneId] then
                table.insert(result, otherPlayer)
            end
        end
    end
    return result
end

NvN.onPlayerConnect = function(playerID) -- "Occurs when a player connects to a server."
    local thisPlayer = net.get_player_info(playerID)
    local _playerName = thisPlayer.name
    local _ucid = thisPlayer.ucid
    logUserUcid(_playerName, _ucid)
end

NvN.onPlayerTryChangeSlot = function(playerID, side, slotId)
    if not DCS.isServer() or not DCS.isMultiplayer() then
        return
    end
    logWriteMessyDebug("onPlayerTryChangeSlot  1 - playerID " .. playerID .. ", side " .. side .. ",slotId " .. slotId)
    NvN.reloadExternalUcidGroups()
    -- fileOutMessyDebug("c:\\tmp\\dcs_available_slots_red.lua", DCS.getAvailableSlots("red"))
    -- fileOutMessyDebug("c:\\tmp\\dcs_available_slots_blue.lua", DCS.getAvailableSlots("blue"))
    -- fileOutMessyDebug("c:\\tmp\\dcs_player_list.lua", net.get_player_list())
    if playerID then
        local thisPlayer = net.get_player_info(playerID)
        local _playerName = thisPlayer.name
        local _ucid = thisPlayer.ucid
        if side == 0 then
            logWriteMessyDebug("Going to side 0 always approved.")
            return
        end
        if string.find(tostring(slotId), '_') then
            logWriteMessyDebug("Slot name contains underscore (_), not handled by this script.")
            return
        end
        local targetUnit = NvN.getMissionUnitBySlotId(slotId)

        for zoneId, isInZone in pairs(targetUnit.inZones or {}) do
            if isInZone then
                local zone = NvN.knownZones[zoneId]
                if zone and zone.limitNvN ~= nil then
                    local playersInZone = NvN.getSameSidePlayersInZone(side, zoneId, thisPlayer.id)
                    if #playersInZone >= zone.limitNvN then
                        logWriteMessyDebug("NvN - Rejected Player Name: [" .. dump(_playerName) .. "] UCID: [" ..
                                               dump(_ucid) .. "] for Slot: " .. dump(slotId) .. ", playersInzone=" ..
                                               #playersInZone .. ", limit=" .. zone.limitNvN)
                        NvN.rejectMessage(playerID, zone.nvnRejectText)
                        return false
                    end
                end
                if zone and zone.ucidGroupName then
                    logWriteMessyDebug("NvN - zone is UCID restricted by ucid group name:" .. zone.ucidGroupName)
                    local ucidList = UCID_GROUPS[zone.ucidGroupName] or {}
                    if isItemInList(thisPlayer.ucid, ucidList) then
                        logWriteMessyDebug("NvN - UCID found. Entry allowed for ucid:" .. thisPlayer.ucid)
                    else
                        NvN.rejectMessage(playerID, zone.ucidRejectText)
                        return false
                    end
                end
            end
        end
        logWriteMessyDebug("AAAA  40")

        -- return false
    end
    return
end

NvN.rejectMessage = function(playerID, customMessage)
    local _playerName = net.get_player_info(playerID, 'name')

    if _playerName ~= nil then
        local _chatMessage = ""

        if customMessage == nil then
            _chatMessage = string.format("*** Sorry %s - Slot CURRENTLY DISABLED - Pick a different slot! ***", _playerName)
        else
            _chatMessage = customMessage
        end
        net.send_chat_to(_chatMessage, playerID)
    end

end

if DCS then
    DCS.setUserCallbacks(NvN)
    logWriteMessyDebug("Loaded - NvN")
end
